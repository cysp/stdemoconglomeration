//
//  STAppDelegate.h
//  STDemo
//
//  Created by Scott Talbot on 23/10/12.
//  Copyright (c) 2012 Scott Talbot. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface STDemoApplicationDelegate : UIResponder <UIApplicationDelegate>
@property (nonatomic, strong) UIWindow *window;
@property (nonatomic, strong, readonly) NSSet *registeredDemoViewControllerClasses;
@end

extern inline STDemoApplicationDelegate *SharedSTDemoApplicationDelegate(void);
