//
//  STDemoViewController.h
//  STDemo
//
//  Created by Scott Talbot on 23/10/12.
//  Copyright (c) 2012 Scott Talbot. All rights reserved.
//

#import <Foundation/Foundation.h>


static NSString * const STDemoRegisterViewControllerClassNotification = @"STDemo.registerViewControllerClass";


@protocol STDemoViewController <NSObject>
@required
+ (NSString *)stDemoTitle;
+ (instancetype)stDemoViewController;
@optional
+ (NSString *)stDemoSubtitle;
@end
