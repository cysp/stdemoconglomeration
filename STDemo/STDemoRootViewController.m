//
//  STDemoRootViewController.m
//  STDemo
//
//  Created by Scott Talbot on 23/10/12.
//  Copyright (c) 2012 Scott Talbot. All rights reserved.
//

#import "STDemoRootViewController.h"

#import "STDemoViewController.h"
#import "STDemoApplicationDelegate.h"


@interface STDemoRootViewController () <UITableViewDataSource,UITableViewDelegate>
@end


@implementation STDemoRootViewController {
	UITableView *_tableView;
	NSMutableArray *_items;
}

+ (instancetype)viewController {
	return [[self alloc] initWithNibName:nil bundle:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		_items = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)loadView {
	UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
	self.view = view;

	_tableView = [[UITableView alloc] initWithFrame:view.bounds];
	_tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
	_tableView.dataSource = self;
	_tableView.delegate = self;
	[self.view addSubview:_tableView];

	__typeof__(self) __weak bself = self;
	dispatch_async(dispatch_get_main_queue(), ^{
		__typeof__(self) self = bself;
		[self reload];
	});
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];

	[_tableView deselectRowAtIndexPath:_tableView.indexPathForSelectedRow animated:YES];
}


- (void)reload {
	NSMutableArray *demoViewControllerClasses = [[SharedSTDemoApplicationDelegate().registeredDemoViewControllerClasses allObjects] mutableCopy];
	[demoViewControllerClasses sortUsingComparator:^NSComparisonResult(Class<STDemoViewController> obj1, Class<STDemoViewController> obj2) {
		NSStringCompareOptions compareOptions = NSCaseInsensitiveSearch|NSNumericSearch|NSDiacriticInsensitiveSearch|NSWidthInsensitiveSearch|NSForcedOrderingSearch;

		NSString *title1 = [obj1 stDemoTitle] ?: @"";
		NSString *title2 = [obj2 stDemoTitle] ?: @"";
		NSComparisonResult result = [title1 compare:title2 options:compareOptions];
		if (result != NSOrderedSame) {
			return result;
		}

		NSString *subtitle1 = [(id)obj1 respondsToSelector:@selector(stDemoSubtitle)] ? [obj1 stDemoSubtitle] : @"";
		NSString *subtitle2 = [(id)obj2 respondsToSelector:@selector(stDemoSubtitle)] ? [obj2 stDemoSubtitle] : @"";
		return [subtitle1 compare:subtitle2 options:compareOptions];
	}];

	[_items setArray:demoViewControllerClasses];
	[_tableView reloadData];
}


#pragma mark - UITableViewDataSource/UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [_items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	Class<STDemoViewController> viewControllerClass = [_items objectAtIndex:indexPath.row];

	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"."];
	if (!cell) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"."];
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	}

	cell.textLabel.text = [viewControllerClass stDemoTitle];
	if ([(id)viewControllerClass respondsToSelector:@selector(stDemoSubtitle)]) {
		cell.detailTextLabel.text = [viewControllerClass stDemoSubtitle];
	}

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	Class<STDemoViewController> viewControllerClass = [_items objectAtIndex:indexPath.row];

	UIViewController<STDemoViewController> *viewController = (UIViewController<STDemoViewController> *)[viewControllerClass stDemoViewController];

	[self.navigationController pushViewController:viewController animated:YES];
}

@end
