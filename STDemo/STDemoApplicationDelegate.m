//
//  STAppDelegate.m
//  STDemo
//
//  Created by Scott Talbot on 23/10/12.
//  Copyright (c) 2012 Scott Talbot. All rights reserved.
//

#import "STDemoApplicationDelegate.h"

#import "STDemoViewController.h"
#import "STDemoRootViewController.h"



@implementation STDemoApplicationDelegate {
	NSMutableSet *_registeredDemoViewControllerClasses;
}

- (id)init {
	if ((self = [super init])) {
		_registeredDemoViewControllerClasses = [[NSMutableSet alloc] init];

		[[NSNotificationCenter defaultCenter] addObserverForName:STDemoRegisterViewControllerClassNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
			UIViewController * const viewController = note.object;
			if (viewController) {
				[_registeredDemoViewControllerClasses addObject:viewController];
			}
		}];
	}
	return self;
}

- (void)setWindow:(UIWindow *)window {
	_window = window;
	[_window makeKeyAndVisible];
}

- (NSSet *)registeredDemoViewControllerClasses {
	return _registeredDemoViewControllerClasses;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	UIScreen *mainScreen = [UIScreen mainScreen];
    UIWindow *window = [[UIWindow alloc] initWithFrame:[mainScreen bounds]];
    window.backgroundColor = [UIColor whiteColor];

	STDemoRootViewController *viewController = [STDemoRootViewController viewController];
	UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
	window.rootViewController = navigationController;

	self.window = window;

    return YES;
}

@end


extern inline STDemoApplicationDelegate *SharedSTDemoApplicationDelegate(void) {
	return [[UIApplication sharedApplication] delegate];
}
