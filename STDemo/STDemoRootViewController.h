//
//  STDemoRootViewController.h
//  STDemo
//
//  Created by Scott Talbot on 23/10/12.
//  Copyright (c) 2012 Scott Talbot. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface STDemoRootViewController : UIViewController
+ (instancetype)viewController;
@end
